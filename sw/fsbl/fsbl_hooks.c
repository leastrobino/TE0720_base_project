/*******************************************************************************
 * Title      : TE0720 FSBL hooks
 * Project    :
 *******************************************************************************
 * File       : fsbl_hooks.c
 * Author     : Lea Strobino <lea.strobino@cern.ch>
 * Company    : CERN - European Organization for Nuclear Research
 * Created    : 22/03/2021
 * Last update:
 * Platform   :
 * Standard   : ANSI C
 *******************************************************************************
 * Description: Configures the TE0720 System Controller and on-board peripherals
 *              before bitstream download.
 *******************************************************************************
 * Copyright (c) 2021 CERN
 * SPDX-License-Identifier: CERN-OHL-W-2.0
 *******************************************************************************
 * Revisions  :
 * Date        Version  Author        Description
 * 22/03/2021  1.0      Lea Strobino  Created
 ******************************************************************************/

#include "fsbl.h"
#include "xstatus.h"
#include "fsbl_hooks.h"

#include "xemacps.h"
#include "xparameters.h"

#define OCM_ENV_ADDR 0xFFFFFC00

#define assert(code) { \
  if ((code) != XST_SUCCESS) return XST_FAILURE; \
}

static void setenv(const char *name, const char *data) {
  static u8 *ptr = (u8*)OCM_ENV_ADDR;
  while (*name) *ptr++ = *name++;
  *ptr++ = '=';
  while (*data) *ptr++ = *data++;
  *ptr++ = 0;
  *ptr   = 0;
}

static char uint4_to_hex(u8 x) {
  if (x < 10) return '0' + x;
  else return ('a' - 10) + x;
}

static void uint8_to_hex(char *s, u8 x) {
  s[0] = uint4_to_hex((x >> 4) & 0x0F);
  s[1] = uint4_to_hex((x >> 0) & 0x0F);
}

static XEmacPs emac;

/*******************************************************************************
 * FSBL hook function which is called before bitstream download
 ******************************************************************************/
u32 FsblHookBeforeBitstreamDload(void) {

  fsbl_printf(DEBUG_INFO, "In FsblHookBeforeBitstreamDload function\r\n");

  xil_printf("\r\n\e[2J\e[H"
    "Xilinx FSBL, release %d.%d\r\n"
    "Build date: " __DATE__ " " __TIME__ "\r\n\r\n",
    SDK_RELEASE_YEAR, SDK_RELEASE_QUARTER
  );

  u8  i, mac_addr[6], sc_rev;
  u16 data;

  char byte[5] = "0x", str[18] = "TE0720-00-";
  static const char temp_grade[4] = "CEIA";

  /* Configure Ethernet driver */

  XEmacPs_Config *cfg = XEmacPs_LookupConfig(XPAR_PS7_ETHERNET_0_DEVICE_ID);
  if (cfg == NULL) return XST_FAILURE;

  assert(XEmacPs_CfgInitialize(&emac, cfg, cfg->BaseAddress));

  /* Read SoM identifier */

  assert(XEmacPs_PhyRead(&emac, 0x1A, 0x03, &data));
  uint8_to_hex(&str[7], (data >> 10) & 0x0F);

  assert(XEmacPs_PhyRead(&emac, 0x1A, 0x04, &data));
  str[10] = uint4_to_hex((data >> 14) & 0x03);
  str[11] = temp_grade[(data >> 12) & 0x03];
  switch ((data >> 8) & 0x0F) {
    case 0: str[12] = 'F'; break;
    case 1: str[12] = 'R'; break;
    case 2: str[12] = 'L'; str[13] = 'F'; break;
    case 3: str[12] = '1'; str[13] = '4'; str[14] = 'S'; break;
  }
  sc_rev = data & 0xFF;
  uint8_to_hex(&byte[2], sc_rev);

  setenv("board_name", str);
  setenv("sc_rev", byte);
  xil_printf("SoM: %s, SC REV%02d\r\n", str, sc_rev);

  /* Read MAC address and configure Ethernet MAC */

  for (i=0; i<3; i++) {
    assert(XEmacPs_PhyRead(&emac, 0x1A, 0x09 + i, &data));
    mac_addr[2*i + 0] = (data >> 8) & 0xFF;
    mac_addr[2*i + 1] = (data >> 0) & 0xFF;
  }

  assert(XEmacPs_SetMacAddress(&emac, mac_addr, 1));

  for (i=0; i<6; i++) {
    uint8_to_hex(&str[3*i], mac_addr[i]);
    str[3*i + 2] = ':';
  }
  str[17] = 0;

  setenv("ethaddr", str);
  xil_printf("MAC: %s\r\n\r\n", str);

  /* Configure Ethernet PHY */

  /* Select page 18 */
  assert(XEmacPs_PhyWrite(&emac, 0x00, 0x16, 0x0012));
  /* General Control Register 1: reset Ethernet PHY, MODE = RGMII to copper */
  assert(XEmacPs_PhyWrite(&emac, 0x00, 0x14, 0x8200));

  /* Select page 3 */
  assert(XEmacPs_PhyWrite(&emac, 0x00, 0x16, 0x0003));
  /* LED Function Control Register:
   *   LED[0] = on: link, off: no link
   *   LED[1] = on: link, blink: activity, off: no link */
  assert(XEmacPs_PhyWrite(&emac, 0x00, 0x10, 0x0010));
  /* LED Polarity Control Register: push-pull, active high */
  assert(XEmacPs_PhyWrite(&emac, 0x00, 0x11, 0x4415));
  /* LED Timer Control Register: LED[2] = IRQ, active low */
  assert(XEmacPs_PhyWrite(&emac, 0x00, 0x12, 0x4985));

  /* Select page 0 */
  assert(XEmacPs_PhyWrite(&emac, 0x00, 0x16, 0x0000));

  /* Configure System Controller */

  /* Control Register 2: XIO4 = SHA_IO, XIO5 = Z, XIO6 = IRQ, XCLK = CLK_125MHZ */
  assert(XEmacPs_PhyWrite(&emac, 0x1A, 0x06, 0x0762));
  /* Confirm XIO5 is configured as input */
  assert(XEmacPs_PhyRead(&emac, 0x1A, 0x06, &data));
  if ((data & 0x00F0) != 0x0060) return XST_FAILURE;

  /* Reset USB PHY */
  assert(XEmacPs_PhyWrite(&emac, 0x1A, 0x07, 0x0010));
  assert(XEmacPs_PhyWrite(&emac, 0x1A, 0x07, 0x0000));

  /* Load bitstream */

  xil_printf("Loading bitstream...\r\n");

  return XST_SUCCESS;

}

/*******************************************************************************
 * FSBL hook function which is called after bitstream download
 ******************************************************************************/
u32 FsblHookAfterBitstreamDload(void) {

  fsbl_printf(DEBUG_INFO, "In FsblHookAfterBitstreamDload function\r\n");

  return XST_SUCCESS;

}

/*******************************************************************************
 * FSBL hook function which is called before handoff to the application
 ******************************************************************************/
u32 FsblHookBeforeHandoff(void) {

  fsbl_printf(DEBUG_INFO, "In FsblHookBeforeHandoff function\r\n");

  xil_printf("Loading U-Boot...\r\n");

  return XST_SUCCESS;

}

/*******************************************************************************
 * FSBL hook function which is called in FSBL fallback
 ******************************************************************************/
void FsblHookFallback(void) {

  fsbl_printf(DEBUG_INFO, "In FsblHookFallback function\r\n");

  while (1);

}
