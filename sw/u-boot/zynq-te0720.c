/*******************************************************************************
 * Title      : TE0720 board initialization for U-Boot
 * Project    :
 *******************************************************************************
 * File       : zynq-te0720.c
 * Author     : Lea Strobino <lea.strobino@cern.ch>
 * Company    : CERN - European Organization for Nuclear Research
 * Created    : 22/03/2021
 * Last update:
 * Platform   :
 * Standard   : ANSI C
 *******************************************************************************
 * Description: Imports the environment saved in OCM by FSBL.
 *******************************************************************************
 * Copyright (c) 2021 CERN
 * SPDX-License-Identifier: CERN-OHL-W-2.0
 *******************************************************************************
 * Revisions  :
 * Date        Version  Author        Description
 * 22/03/2021  1.0      Lea Strobino  Created
 ******************************************************************************/

#include <env_internal.h>
#include <search.h>

#define OCM_ENV_ADDR 0xFFFFFC00
#define OCM_ENV_SIZE 0x200

#ifdef CONFIG_MISC_INIT_R
int misc_init_r(void) {
  return !himport_r(
    &env_htab, (const char*)OCM_ENV_ADDR,
    OCM_ENV_SIZE, 0, H_NOCLEAR, 0, 0, NULL
  );
}
#endif
