/*******************************************************************************
 * Title      : TE0720 board configuration for U-Boot
 * Project    :
 *******************************************************************************
 * File       : zynq-te0720.h
 * Author     : Lea Strobino <lea.strobino@cern.ch>
 * Company    : CERN - European Organization for Nuclear Research
 * Created    : 22/03/2021
 * Last update:
 * Platform   :
 * Standard   : ANSI C
 *******************************************************************************
 * Description: TE0720 specific default environment and pre-boot hooks
 *******************************************************************************
 * Copyright (c) 2021 CERN
 * SPDX-License-Identifier: CERN-OHL-W-2.0
 *******************************************************************************
 * Revisions  :
 * Date        Version  Author        Description
 * 22/03/2021  1.0      Lea Strobino  Created
 ******************************************************************************/

#ifndef __CONFIG_ZYNQ_TE0720_H
#define __CONFIG_ZYNQ_TE0720_H

#include <configs/zynq-common.h>

#undef CONFIG_EXTRA_ENV_SETTINGS
#undef CONFIG_PREBOOT

#if !CONFIG_IS_ENABLED(DFU)
# undef  DFU_ALT_INFO
# define DFU_ALT_INFO
#endif

/* Ethernet PHY auto-negotiation timeout: 5000 ms */
#define PHY_ANEG_TIMEOUT 100

/* System Controller configuration */
/* Control Register 1: LED1 (green) = MIO7, LED2 (red) = 0, NOSEQ = PHY_LED1 */
#define SC_WRITE_CR1 "mii write 0x1a 0x05 0x0264"

/* Power-on reset through System Controller */
#define SC_POR "mii write 0x1a 0x07 0xe570 && mii write 0x1a 0x05 0x8000"

/* Default environment */
#define CONFIG_EXTRA_ENV_SETTINGS \
  "bootm_low=0x0\0" \
  "bootm_size=0x40000000\0" \
  "fdt_addr_r=0x1f00000\0" \
  "kernel_addr_r=0x2000000\0" \
  "pxefile_addr_r=0x2000000\0" \
  "ramdisk_addr_r=0x3100000\0" \
  "resetcmd=" SC_POR "\0" \
  "script_offset_f=" __stringify(CONFIG_BOOT_SCRIPT_OFFSET) "\0" \
  "script_size_f=0x40000\0" \
  "scriptaddr=0x3000000\0" \
  DFU_ALT_INFO \
  BOOTENV

/* Pre-boot hooks */
#define CONFIG_PREBOOT \
  "setenv preboot; " \
  SC_WRITE_CR1 "; " \
  "echo"

#endif /* __CONFIG_ZYNQ_TE0720_H */
