--------------------------------------------------------------------------------
-- Title      : TE0720 top design
-- Project    :
--------------------------------------------------------------------------------
-- File       : TE0720_top.vhd
-- Author     : Lea Strobino <lea.strobino@cern.ch>
-- Company    : CERN - European Organization for Nuclear Research
-- Created    : 22/03/2021
-- Last update:
-- Platform   :
-- Standard   : VHDL'93/02
--------------------------------------------------------------------------------
-- Description:
--------------------------------------------------------------------------------
-- Copyright (c) 2021 CERN
-- SPDX-License-Identifier: CERN-OHL-W-2.0
--------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author        Description
-- 22/03/2021  1.0      Lea Strobino  Created
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
library UNISIM;
use UNISIM.vcomponents.all;

entity TE0720_top is
  port (
    DDR_addr                    : inout std_logic_vector(14 downto 0);
    DDR_ba                      : inout std_logic_vector(2 downto 0);
    DDR_cas_n                   : inout std_logic;
    DDR_ck_p                    : inout std_logic;
    DDR_ck_n                    : inout std_logic;
    DDR_cke                     : inout std_logic;
    DDR_cs_n                    : inout std_logic;
    DDR_dm                      : inout std_logic_vector(3 downto 0);
    DDR_dq                      : inout std_logic_vector(31 downto 0);
    DDR_dqs_p                   : inout std_logic_vector(3 downto 0);
    DDR_dqs_n                   : inout std_logic_vector(3 downto 0);
    DDR_odt                     : inout std_logic;
    DDR_ras_n                   : inout std_logic;
    DDR_reset_n                 : inout std_logic;
    DDR_we_n                    : inout std_logic;
    FIXED_IO_ddr_vrp            : inout std_logic;
    FIXED_IO_ddr_vrn            : inout std_logic;
    FIXED_IO_mio                : inout std_logic_vector(53 downto 0);
    FIXED_IO_ps_clk             : inout std_logic;
    FIXED_IO_ps_porb            : inout std_logic;
    FIXED_IO_ps_srstb           : inout std_logic;
    M2C_LED_o                   : out std_logic_vector(1 downto 0);
    SC_SCL_o                    : out std_logic;
    SC_SDA_i                    : in  std_logic;
    SC_SDA_o                    : out std_logic;
    SC_XIO4_b                   : in  std_logic;
    SC_XIO5_b                   : out std_logic;
    SC_XIO6_b                   : in  std_logic
  );
end entity;

architecture RTL of TE0720_top is

  component TE0720 is
    port (
      DDR_addr                  : inout std_logic_vector(14 downto 0);
      DDR_ba                    : inout std_logic_vector(2 downto 0);
      DDR_cas_n                 : inout std_logic;
      DDR_ck_p                  : inout std_logic;
      DDR_ck_n                  : inout std_logic;
      DDR_cke                   : inout std_logic;
      DDR_cs_n                  : inout std_logic;
      DDR_dm                    : inout std_logic_vector(3 downto 0);
      DDR_dq                    : inout std_logic_vector(31 downto 0);
      DDR_dqs_p                 : inout std_logic_vector(3 downto 0);
      DDR_dqs_n                 : inout std_logic_vector(3 downto 0);
      DDR_odt                   : inout std_logic;
      DDR_ras_n                 : inout std_logic;
      DDR_reset_n               : inout std_logic;
      DDR_we_n                  : inout std_logic;
      FIXED_IO_ddr_vrp          : inout std_logic;
      FIXED_IO_ddr_vrn          : inout std_logic;
      FIXED_IO_mio              : inout std_logic_vector(53 downto 0);
      FIXED_IO_ps_clk           : inout std_logic;
      FIXED_IO_ps_porb          : inout std_logic;
      FIXED_IO_ps_srstb         : inout std_logic;
      LED_o                     : out std_logic_vector(2 downto 0);
      SC_SCL_o                  : out std_logic;
      SC_SDA_i                  : in  std_logic;
      SC_SDA_o                  : out std_logic;
      SC_XIO4_b                 : in  std_logic;
      SC_XIO5_b                 : out std_logic;
      SC_XIO6_b                 : in  std_logic
    );
  end component;

  signal LED                    : std_logic_vector(2 downto 0);
  signal DONE_n                 : std_logic;

begin

  TE0720_bd : component TE0720
  port map (
    DDR_addr                    => DDR_addr,
    DDR_ba                      => DDR_ba,
    DDR_cas_n                   => DDR_cas_n,
    DDR_ck_p                    => DDR_ck_p,
    DDR_ck_n                    => DDR_ck_n,
    DDR_cke                     => DDR_cke,
    DDR_cs_n                    => DDR_cs_n,
    DDR_dm                      => DDR_dm,
    DDR_dq                      => DDR_dq,
    DDR_dqs_p                   => DDR_dqs_p,
    DDR_dqs_n                   => DDR_dqs_n,
    DDR_odt                     => DDR_odt,
    DDR_ras_n                   => DDR_ras_n,
    DDR_reset_n                 => DDR_reset_n,
    DDR_we_n                    => DDR_we_n,
    FIXED_IO_ddr_vrp            => FIXED_IO_ddr_vrp,
    FIXED_IO_ddr_vrn            => FIXED_IO_ddr_vrn,
    FIXED_IO_mio                => FIXED_IO_mio,
    FIXED_IO_ps_clk             => FIXED_IO_ps_clk,
    FIXED_IO_ps_porb            => FIXED_IO_ps_porb,
    FIXED_IO_ps_srstb           => FIXED_IO_ps_srstb,
    LED_o                       => LED,
    SC_SCL_o                    => SC_SCL_o,
    SC_SDA_i                    => SC_SDA_i,
    SC_SDA_o                    => SC_SDA_o,
    SC_XIO4_b                   => SC_XIO4_b,
    SC_XIO5_b                   => SC_XIO5_b,
    SC_XIO6_b                   => SC_XIO6_b
  );

  STARTUPE2_inst : component STARTUPE2
  generic map (
    PROG_USR                    => "FALSE"
  )
  port map (
    CLK                         => '0',
    GSR                         => '0',
    GTS                         => '0',
    KEYCLEARB                   => '1',
    PACK                        => '0',
    USRCCLKO                    => '0',
    USRCCLKTS                   => '1',
    USRDONEO                    => '0',
    USRDONETS                   => DONE_n
  );

  DONE_n                        <= not LED(0);
  M2C_LED_o                     <= LED(1) & LED(2);

end architecture;
