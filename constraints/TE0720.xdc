#*******************************************************************************
#* Title      : TE0720 pin mapping
#* Project    :
#*******************************************************************************
#* File       : TE0720.xdc
#* Author     : Lea Strobino <lea.strobino@cern.ch>
#* Company    : CERN - European Organization for Nuclear Research
#* Created    : 22/03/2021
#* Last update:
#* Platform   : Xilinx XC7Z020-1CLG484C Zynq-7020 SoC
#* Standard   :
#*******************************************************************************
#* Description: Package pins and IO standard for TE0720-03 + TE0703-06
#*******************************************************************************
#* Copyright (c) 2021 CERN
#* SPDX-License-Identifier: CERN-OHL-W-2.0
#*******************************************************************************
#* Revisions  :
#* Date        Version  Author        Description
#* 22/03/2021  1.0      Lea Strobino  Created
#*******************************************************************************

# IO standard

set_property IOSTANDARD LVCMOS33 [get_ports {B13_* M2C_*}]; # Bank 13, VCCIOD
set_property IOSTANDARD LVCMOS33 [get_ports {B33_*}];       # Bank 33, VCCIOC
set_property IOSTANDARD LVCMOS33 [get_ports {B34_* SC_*}];  # Bank 34, VCCIOB
set_property IOSTANDARD LVCMOS33 [get_ports {B35_*}];       # Bank 35, VCCIOA

# Package pins

# Bank 13: 2 IOs
set_property PACKAGE_PIN R7   [get_ports {B13_IO0  M2C_LED_o[0]}];
set_property PACKAGE_PIN U7   [get_ports {B13_IO25 M2C_LED_o[1]}];

# Bank 13: 24 IO pairs
set_property PACKAGE_PIN V10  [get_ports {B13_L1_P}];
set_property PACKAGE_PIN V9   [get_ports {B13_L1_N}];
set_property PACKAGE_PIN V8   [get_ports {B13_L2_P}];
set_property PACKAGE_PIN W8   [get_ports {B13_L2_N}];
set_property PACKAGE_PIN W11  [get_ports {B13_L3_P}];
set_property PACKAGE_PIN W10  [get_ports {B13_L3_N}];
set_property PACKAGE_PIN V12  [get_ports {B13_L4_P}];
set_property PACKAGE_PIN W12  [get_ports {B13_L4_N}];
set_property PACKAGE_PIN U12  [get_ports {B13_L5_P}];
set_property PACKAGE_PIN U11  [get_ports {B13_L5_N}];
set_property PACKAGE_PIN U10  [get_ports {B13_L6_P}];
set_property PACKAGE_PIN U9   [get_ports {B13_L6_N}];
set_property PACKAGE_PIN AA12 [get_ports {B13_L7_P}];
set_property PACKAGE_PIN AB12 [get_ports {B13_L7_N}];
set_property PACKAGE_PIN AA11 [get_ports {B13_L8_P}];
set_property PACKAGE_PIN AB11 [get_ports {B13_L8_N}];
set_property PACKAGE_PIN AB10 [get_ports {B13_L9_P}];
set_property PACKAGE_PIN AB9  [get_ports {B13_L9_N}];
set_property PACKAGE_PIN Y11  [get_ports {B13_L10_P}];
set_property PACKAGE_PIN Y10  [get_ports {B13_L10_N}];
set_property PACKAGE_PIN AA9  [get_ports {B13_L11_P}];
set_property PACKAGE_PIN AA8  [get_ports {B13_L11_N}];
set_property PACKAGE_PIN Y9   [get_ports {B13_L12_P}];
set_property PACKAGE_PIN Y8   [get_ports {B13_L12_N}];
set_property PACKAGE_PIN Y6   [get_ports {B13_L13_P}];
set_property PACKAGE_PIN Y5   [get_ports {B13_L13_N}];
set_property PACKAGE_PIN AA7  [get_ports {B13_L14_P}];
set_property PACKAGE_PIN AA6  [get_ports {B13_L14_N}];
set_property PACKAGE_PIN AB2  [get_ports {B13_L15_P}];
set_property PACKAGE_PIN AB1  [get_ports {B13_L15_N}];
set_property PACKAGE_PIN AB5  [get_ports {B13_L16_P}];
set_property PACKAGE_PIN AB4  [get_ports {B13_L16_N}];
set_property PACKAGE_PIN AB7  [get_ports {B13_L17_P}];
set_property PACKAGE_PIN AB6  [get_ports {B13_L17_N}];
set_property PACKAGE_PIN Y4   [get_ports {B13_L18_P}];
set_property PACKAGE_PIN AA4  [get_ports {B13_L18_N}];
set_property PACKAGE_PIN R6   [get_ports {B13_L19_P}];
set_property PACKAGE_PIN T6   [get_ports {B13_L19_N}];
set_property PACKAGE_PIN T4   [get_ports {B13_L20_P}];
set_property PACKAGE_PIN U4   [get_ports {B13_L20_N}];
set_property PACKAGE_PIN V5   [get_ports {B13_L21_P}];
set_property PACKAGE_PIN V4   [get_ports {B13_L21_N}];
set_property PACKAGE_PIN U6   [get_ports {B13_L22_P}];
set_property PACKAGE_PIN U5   [get_ports {B13_L22_N}];
set_property PACKAGE_PIN V7   [get_ports {B13_L23_P}];
set_property PACKAGE_PIN W7   [get_ports {B13_L23_N}];
set_property PACKAGE_PIN W6   [get_ports {B13_L24_P}];
set_property PACKAGE_PIN W5   [get_ports {B13_L24_N}];

# Bank 33: 18 IO pairs
set_property PACKAGE_PIN W20  [get_ports {B33_L4_P}];
set_property PACKAGE_PIN W21  [get_ports {B33_L4_N}];
set_property PACKAGE_PIN AA22 [get_ports {B33_L7_P}];
set_property PACKAGE_PIN AB22 [get_ports {B33_L7_N}];
set_property PACKAGE_PIN AA21 [get_ports {B33_L8_P}];
set_property PACKAGE_PIN AB21 [get_ports {B33_L8_N}];
set_property PACKAGE_PIN Y19  [get_ports {B33_L11_P}];
set_property PACKAGE_PIN AA19 [get_ports {B33_L11_N}];
set_property PACKAGE_PIN Y18  [get_ports {B33_L12_P}];
set_property PACKAGE_PIN AA18 [get_ports {B33_L12_N}];
set_property PACKAGE_PIN W17  [get_ports {B33_L13_P}];
set_property PACKAGE_PIN W18  [get_ports {B33_L13_N}];
set_property PACKAGE_PIN W16  [get_ports {B33_L14_P}];
set_property PACKAGE_PIN Y16  [get_ports {B33_L14_N}];
set_property PACKAGE_PIN AA17 [get_ports {B33_L17_P}];
set_property PACKAGE_PIN AB17 [get_ports {B33_L17_N}];
set_property PACKAGE_PIN AA16 [get_ports {B33_L18_P}];
set_property PACKAGE_PIN AB16 [get_ports {B33_L18_N}];

# Bank 34: 36 IO pairs
set_property PACKAGE_PIN J15  [get_ports {B34_L1_P}];
set_property PACKAGE_PIN K15  [get_ports {B34_L1_N}];
set_property PACKAGE_PIN J16  [get_ports {B34_L2_P}];
set_property PACKAGE_PIN J17  [get_ports {B34_L2_N}];
set_property PACKAGE_PIN L17  [get_ports {B34_L4_P}];
set_property PACKAGE_PIN M17  [get_ports {B34_L4_N}];
set_property PACKAGE_PIN N17  [get_ports {B34_L5_P}];
set_property PACKAGE_PIN N18  [get_ports {B34_L5_N}];
set_property PACKAGE_PIN J18  [get_ports {B34_L7_P}];
set_property PACKAGE_PIN K18  [get_ports {B34_L7_N}];
set_property PACKAGE_PIN J21  [get_ports {B34_L8_P}];
set_property PACKAGE_PIN J22  [get_ports {B34_L8_N}];
set_property PACKAGE_PIN J20  [get_ports {B34_L9_P}];
set_property PACKAGE_PIN K21  [get_ports {B34_L9_N}];
set_property PACKAGE_PIN L21  [get_ports {B34_L10_P}];
set_property PACKAGE_PIN L22  [get_ports {B34_L10_N}];
set_property PACKAGE_PIN L18  [get_ports {B34_L12_P}];
set_property PACKAGE_PIN L19  [get_ports {B34_L12_N}];
set_property PACKAGE_PIN M19  [get_ports {B34_L13_P}];
set_property PACKAGE_PIN M20  [get_ports {B34_L13_N}];
set_property PACKAGE_PIN N19  [get_ports {B34_L14_P}];
set_property PACKAGE_PIN N20  [get_ports {B34_L14_N}];
set_property PACKAGE_PIN M21  [get_ports {B34_L15_P}];
set_property PACKAGE_PIN M22  [get_ports {B34_L15_N}];
set_property PACKAGE_PIN R20  [get_ports {B34_L17_P}];
set_property PACKAGE_PIN R21  [get_ports {B34_L17_N}];
set_property PACKAGE_PIN P20  [get_ports {B34_L18_P}];
set_property PACKAGE_PIN P21  [get_ports {B34_L18_N}];
set_property PACKAGE_PIN P17  [get_ports {B34_L20_P}];
set_property PACKAGE_PIN P18  [get_ports {B34_L20_N}];
set_property PACKAGE_PIN T16  [get_ports {B34_L21_P}];
set_property PACKAGE_PIN T17  [get_ports {B34_L21_N}];
set_property PACKAGE_PIN R19  [get_ports {B34_L22_P}];
set_property PACKAGE_PIN T19  [get_ports {B34_L22_N}];
set_property PACKAGE_PIN R18  [get_ports {B34_L23_P}];
set_property PACKAGE_PIN T18  [get_ports {B34_L23_N}];

# Bank 35: 48 IO pairs
set_property PACKAGE_PIN F16  [get_ports {B35_L1_P}];
set_property PACKAGE_PIN E16  [get_ports {B35_L1_N}];
set_property PACKAGE_PIN D16  [get_ports {B35_L2_P}];
set_property PACKAGE_PIN D17  [get_ports {B35_L2_N}];
set_property PACKAGE_PIN E15  [get_ports {B35_L3_P}];
set_property PACKAGE_PIN D15  [get_ports {B35_L3_N}];
set_property PACKAGE_PIN G15  [get_ports {B35_L4_P}];
set_property PACKAGE_PIN G16  [get_ports {B35_L4_N}];
set_property PACKAGE_PIN F18  [get_ports {B35_L5_P}];
set_property PACKAGE_PIN E18  [get_ports {B35_L5_N}];
set_property PACKAGE_PIN G17  [get_ports {B35_L6_P}];
set_property PACKAGE_PIN F17  [get_ports {B35_L6_N}];
set_property PACKAGE_PIN C15  [get_ports {B35_L7_P}];
set_property PACKAGE_PIN B15  [get_ports {B35_L7_N}];
set_property PACKAGE_PIN B16  [get_ports {B35_L8_P}];
set_property PACKAGE_PIN B17  [get_ports {B35_L8_N}];
set_property PACKAGE_PIN A16  [get_ports {B35_L9_P}];
set_property PACKAGE_PIN A17  [get_ports {B35_L9_N}];
set_property PACKAGE_PIN A18  [get_ports {B35_L10_P}];
set_property PACKAGE_PIN A19  [get_ports {B35_L10_N}];
set_property PACKAGE_PIN C17  [get_ports {B35_L11_P}];
set_property PACKAGE_PIN C18  [get_ports {B35_L11_N}];
set_property PACKAGE_PIN D18  [get_ports {B35_L12_P}];
set_property PACKAGE_PIN C19  [get_ports {B35_L12_N}];
set_property PACKAGE_PIN B19  [get_ports {B35_L13_P}];
set_property PACKAGE_PIN B20  [get_ports {B35_L13_N}];
set_property PACKAGE_PIN D20  [get_ports {B35_L14_P}];
set_property PACKAGE_PIN C20  [get_ports {B35_L14_N}];
set_property PACKAGE_PIN A21  [get_ports {B35_L15_P}];
set_property PACKAGE_PIN A22  [get_ports {B35_L15_N}];
set_property PACKAGE_PIN D22  [get_ports {B35_L16_P}];
set_property PACKAGE_PIN C22  [get_ports {B35_L16_N}];
set_property PACKAGE_PIN E21  [get_ports {B35_L17_P}];
set_property PACKAGE_PIN D21  [get_ports {B35_L17_N}];
set_property PACKAGE_PIN B21  [get_ports {B35_L18_P}];
set_property PACKAGE_PIN B22  [get_ports {B35_L18_N}];
set_property PACKAGE_PIN H19  [get_ports {B35_L19_P}];
set_property PACKAGE_PIN H20  [get_ports {B35_L19_N}];
set_property PACKAGE_PIN G19  [get_ports {B35_L20_P}];
set_property PACKAGE_PIN F19  [get_ports {B35_L20_N}];
set_property PACKAGE_PIN E19  [get_ports {B35_L21_P}];
set_property PACKAGE_PIN E20  [get_ports {B35_L21_N}];
set_property PACKAGE_PIN G20  [get_ports {B35_L22_P}];
set_property PACKAGE_PIN G21  [get_ports {B35_L22_N}];
set_property PACKAGE_PIN F21  [get_ports {B35_L23_P}];
set_property PACKAGE_PIN F22  [get_ports {B35_L23_N}];
set_property PACKAGE_PIN H22  [get_ports {B35_L24_P}];
set_property PACKAGE_PIN G22  [get_ports {B35_L24_N}];

# XADC (bank 35)
set_property PACKAGE_PIN L11  [get_ports {XADC_VP_i}];
set_property PACKAGE_PIN M12  [get_ports {XADC_VN_i}];
set_property PACKAGE_PIN F16  [get_ports {XADC_VAUX_P_i[0]}];
set_property PACKAGE_PIN E15  [get_ports {XADC_VAUX_P_i[1]}];
set_property PACKAGE_PIN C15  [get_ports {XADC_VAUX_P_i[2]}];
set_property PACKAGE_PIN A16  [get_ports {XADC_VAUX_P_i[3]}];
set_property PACKAGE_PIN D20  [get_ports {XADC_VAUX_P_i[4]}];
set_property PACKAGE_PIN E21  [get_ports {XADC_VAUX_P_i[5]}];
set_property PACKAGE_PIN G19  [get_ports {XADC_VAUX_P_i[6]}];
set_property PACKAGE_PIN G20  [get_ports {XADC_VAUX_P_i[7]}];
set_property PACKAGE_PIN D16  [get_ports {XADC_VAUX_P_i[8]}];
set_property PACKAGE_PIN F18  [get_ports {XADC_VAUX_P_i[9]}];
set_property PACKAGE_PIN B16  [get_ports {XADC_VAUX_P_i[10]}];
set_property PACKAGE_PIN A18  [get_ports {XADC_VAUX_P_i[11]}];
set_property PACKAGE_PIN A21  [get_ports {XADC_VAUX_P_i[12]}];
set_property PACKAGE_PIN B21  [get_ports {XADC_VAUX_P_i[13]}];
set_property PACKAGE_PIN E19  [get_ports {XADC_VAUX_P_i[14]}];
set_property PACKAGE_PIN H22  [get_ports {XADC_VAUX_P_i[15]}];
set_property PACKAGE_PIN E16  [get_ports {XADC_VAUX_N_i[0]}];
set_property PACKAGE_PIN D15  [get_ports {XADC_VAUX_N_i[1]}];
set_property PACKAGE_PIN B15  [get_ports {XADC_VAUX_N_i[2]}];
set_property PACKAGE_PIN A17  [get_ports {XADC_VAUX_N_i[3]}];
set_property PACKAGE_PIN C20  [get_ports {XADC_VAUX_N_i[4]}];
set_property PACKAGE_PIN D21  [get_ports {XADC_VAUX_N_i[5]}];
set_property PACKAGE_PIN F19  [get_ports {XADC_VAUX_N_i[6]}];
set_property PACKAGE_PIN G21  [get_ports {XADC_VAUX_N_i[7]}];
set_property PACKAGE_PIN D17  [get_ports {XADC_VAUX_N_i[8]}];
set_property PACKAGE_PIN E18  [get_ports {XADC_VAUX_N_i[9]}];
set_property PACKAGE_PIN B17  [get_ports {XADC_VAUX_N_i[10]}];
set_property PACKAGE_PIN A19  [get_ports {XADC_VAUX_N_i[11]}];
set_property PACKAGE_PIN A22  [get_ports {XADC_VAUX_N_i[12]}];
set_property PACKAGE_PIN B22  [get_ports {XADC_VAUX_N_i[13]}];
set_property PACKAGE_PIN E20  [get_ports {XADC_VAUX_N_i[14]}];
set_property PACKAGE_PIN G22  [get_ports {XADC_VAUX_N_i[15]}];

# System Controller (bank 34)
set_property PACKAGE_PIN K19  [get_ports {SC_X[0] SC_XCLK_i}];
set_property PACKAGE_PIN L16  [get_ports {SC_X[1] SC_SCL_o}];
set_property PACKAGE_PIN M15  [get_ports {SC_X[2] SC_XIO4_b}];
set_property PACKAGE_PIN N15  [get_ports {SC_X[3] SC_XIO5_b}];
set_property PACKAGE_PIN P16  [get_ports {SC_X[4] SC_XIO6_b}];
set_property PACKAGE_PIN P22  [get_ports {SC_X[5] SC_SDA_i}];
set_property PACKAGE_PIN K20  [get_ports {SC_X[6]}];
set_property PACKAGE_PIN N22  [get_ports {SC_X[7] SC_SDA_o}];
